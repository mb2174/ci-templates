# CI templates

This repository contains a set of templates intended to be included into CI jobs
on GitLab. See the comments at the top of each template for usage information.
