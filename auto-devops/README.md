# Auto DevOps customisations

This directory holds templates which are designed to override or extend the
built-in GitLab Auto DevOps flow.
